<?php

class Session{
    
        public static function init() {
            @session_start();
	}
        
        public static function show() {
            return var_dump($_SESSION);
        }
	
	public static function set($key, $value) {
            $_SESSION[$key] = $value;
	}
        
        public static function addcount($key){
            if(isset($_SESSION[$key])){
                $_SESSION[$key]++;
            }else{
                $_SESSION[$key] = 0;
            }
        }
	
	public static function get($key) {
            if(isset($_SESSION[$key])){
                return $_SESSION[$key];
            }
	}
        
        public static function clear($key = ''){
            session_unset();
            Session::set($key,0);
        }
	
	public static function destroy() {
            session_destroy();
	}
}

