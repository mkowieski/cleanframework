<?php

class Controller{
    
    private $model;
    
    public function model($model){
        require_once 'app/model/' . $model . '.php';
        return new $model();
    }
    
    public function view($view, $content = true, $data = ''){
        if($content == true){
            require_once 'app/view/header.php';
            require_once 'app/view/'. $view . '.php';
            require_once 'app/view/footer.php';
        }
        
        require_once 'app/view/'. $view . '.php';
    }
    
}

