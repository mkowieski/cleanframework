<?php

class ErrorController extends Controller{
    
    public function __construct(){
        
    }
    
    public function fileNotFound(){
        $this->view('error/404', false);
    }
    
}
